       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. Mankhong.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MYGRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  MYGRADE-FILE.
       01  MYGRADE-DETAIL.
           88 END-OF-MYGRADE-FILE VALUE HIGH-VALUE .
           05 COURSE-CODE  PIC X(6).
           05 COURSE-NAME  PIC X(50).
           05 CREDIT       PIC 9(1).
           05 GRADE        PIC X(2).
       FD  AVG-FILE.
       01  AVG-GRADE   PIC 9(1)V9(3) . 
       01  AVG-SCI-GRADE  PIC 9(1)V9(3) .
       01  AVG-CS-GRADE  PIC 9(1)V9(3) .
       WORKING-STORAGE SECTION. 
       01  SUM-GRADE   PIC 9(3)V9(3) VALUE ZERO .    
       01  SUM-CREDIT  PIC 9(3) VALUE ZERO .
       01  SUM-SCI-GRADE PIC 9(3)V9(3) VALUE ZERO . 
       01  SUM-SCI-CREDIT  PIC 9(3) VALUE ZERO .
       01  SUM-CS-GRADE PIC 9(3)V9(3) VALUE ZERO . 
       01  SUM-CS-CREDIT  PIC 9(3) VALUE ZERO .
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT MYGRADE-FILE 
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-MYGRADE-FILE 
              READ MYGRADE-FILE 
                 AT END SET END-OF-MYGRADE-FILE TO  TRUE 
               END-READ
               IF NOT END-OF-MYGRADE-FILE THEN                    
                    PERFORM 001-PROCESS-SUMGRADE THRU 001-EXIT 
                    PERFORM 002-PROCESS-SUMCREDIT  THRU 002-EXIT 
                    PERFORM 004-SUM-SCI-GRADE THRU 004-EXIT   
                    PERFORM 005-PROCESS-SCI-SUMCREDIT THRU 005-EXIT
                    PERFORM 007-SUM-CS-GRADE THRU 007-EXIT   
                    PERFORM 008-PROCESS-CS-SUMCREDIT THRU 008-EXIT              
                    
               END-IF
           END-PERFORM           
           PERFORM 003-PROCESS-AVG-GRADE THRU 003-EXIT 
           PERFORM 006-PROCESS-AVG-SCI-GRADE THRU 006-EXIT 
           PERFORM 009-PROCESS-AVG-CS-GRADE THRU 009-EXIT
           CLOSE MYGRADE-FILE 
           CLOSE AVG-FILE 
           GOBACK .
       001-PROCESS-SUMGRADE.  
           IF GRADE = "A" THEN              
              COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 4)
           ELSE
              IF GRADE = "B+" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 3.5)
              END-IF
              IF GRADE = "B" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 3)
              END-IF
              IF GRADE = "C+" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 2.5)
              END-IF
              IF GRADE = "C" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 2)
              END-IF
              IF GRADE = "D+" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 1.5)
              END-IF
              IF GRADE = "D" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 1)
              END-IF
              IF GRADE = "F" THEN              
                 COMPUTE  SUM-GRADE = SUM-GRADE + (CREDIT * 0)
              END-IF
           END-IF 
           .
       001-EXIT.
           EXIT 
           .
       002-PROCESS-SUMCREDIT.
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT 
           .
       002-EXIT.
           EXIT
           .
       003-PROCESS-AVG-GRADE.
           COMPUTE AVG-GRADE = SUM-GRADE / SUM-CREDIT 
           WRITE   AVG-GRADE 
           .
       003-EXIT.
           EXIT 
           . 
       004-SUM-SCI-GRADE.
           IF COURSE-CODE(1:1) = "3" THEN
              IF GRADE = "A" THEN              
              COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + (CREDIT * 4)
              ELSE
               IF GRADE = "B+" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + 
                  (CREDIT * 3.5)
               END-IF
               IF GRADE = "B" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + (CREDIT * 3)
               END-IF
               IF GRADE = "C+" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + 
                  (CREDIT * 2.5)
               END-IF
               IF GRADE = "C" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + (CREDIT * 2)
               END-IF
               IF GRADE = "D+" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE +
                   (CREDIT * 1.5)
               END-IF
               IF GRADE = "D" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + (CREDIT * 1)
               END-IF
               IF GRADE = "F" THEN              
                  COMPUTE  SUM-SCI-GRADE = SUM-SCI-GRADE + (CREDIT * 0)
               END-IF
           END-IF
           .
       004-EXIT    .
           EXIT.

       005-PROCESS-SCI-SUMCREDIT.
           IF COURSE-CODE(1:1) = "3" THEN
              COMPUTE SUM-SCI-CREDIT = SUM-SCI-CREDIT + CREDIT
           END-IF 
           .
       005-EXIT.
           EXIT
           . 
       006-PROCESS-AVG-SCI-GRADE.
           COMPUTE AVG-SCI-GRADE = SUM-SCI-GRADE / SUM-SCI-CREDIT 
           WRITE     AVG-SCI-GRADE 
           .
       006-EXIT.
           EXIT 
           .
       007-SUM-CS-GRADE.
           IF COURSE-CODE(1:2) = "31" THEN
              IF GRADE = "A" THEN              
              COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + (CREDIT * 4)
              ELSE
               IF GRADE = "B+" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + 
                  (CREDIT * 3.5)
               END-IF
               IF GRADE = "B" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + (CREDIT * 3)
               END-IF
               IF GRADE = "C+" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + 
                  (CREDIT * 2.5)
               END-IF
               IF GRADE = "C" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + (CREDIT * 2)
               END-IF
               IF GRADE = "D+" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE +
                   (CREDIT * 1.5)
               END-IF
               IF GRADE = "D" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + (CREDIT * 1)
               END-IF
               IF GRADE = "F" THEN              
                  COMPUTE  SUM-CS-GRADE = SUM-CS-GRADE + (CREDIT * 0)
               END-IF
           END-IF
           .
       007-EXIT.
           EXIT.

       008-PROCESS-CS-SUMCREDIT.
           IF COURSE-CODE(1:2) = "31" THEN
              COMPUTE SUM-CS-CREDIT = SUM-CS-CREDIT + CREDIT
           END-IF 
           .
       008-EXIT.
           EXIT
           . 
       009-PROCESS-AVG-CS-GRADE.
           COMPUTE AVG-CS-GRADE = SUM-CS-GRADE / SUM-CS-CREDIT 
           WRITE   AVG-CS-GRADE 
           .
       009-EXIT.
           EXIT 
           .
       
